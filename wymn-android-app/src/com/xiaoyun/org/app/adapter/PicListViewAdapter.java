/**
 * 
 */
package com.xiaoyun.org.app.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.xiaoyun.org.R;
import com.xiaoyun.org.app.AppContext;
import com.xiaoyun.org.app.bean.ImageData;
import com.xiaoyun.org.app.ui.ImageScaleActivity;

/**
 * 摇一摇listview适配器
 * 
 * @author yuanxy
 * 
 */
public class PicListViewAdapter extends BaseAdapter {

	protected final int mItemLayoutId;
	private Context context;
	private LayoutInflater inflater;
	private AppContext appContext;
	private ImageScaleType imageScaleType;
	private DisplayImageOptions options;
	private ViewHolder holder;

	private List<ImageData> data = new ArrayList<ImageData>();

	public PicListViewAdapter(Context context, int itemLayoutId,
			ImageScaleType imageScaleType) {
		this.context = context;
		this.mItemLayoutId = itemLayoutId;
		this.inflater = LayoutInflater.from(context);
		this.appContext = (AppContext) context.getApplicationContext();
		this.imageScaleType = imageScaleType;

		options = new DisplayImageOptions.Builder()
				// .showImageOnFail(R.drawable.ic_error)
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.considerExifParams(true)
				.imageScaleType(
						imageScaleType != null ? imageScaleType
								: ImageScaleType.EXACTLY_STRETCHED)
				.delayBeforeLoading(10).displayer(new SimpleBitmapDisplayer())// //正常显示一张图片　
				.build();
	}

	public void setData(List<ImageData> data) {
		this.data = data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data != null ? data.size() - 1 : 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data != null ? data.get(position) : null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		// View imageLayout = inflater.inflate(R.layout.item, parent, false);
		// assert imageLayout != null;
		// final ProgressBar spinner = (ProgressBar)
		// imageLayout.findViewById(R.id.loading);
		holder = getViewHolder(position, convertView, parent);

		if (data != null && data.size() > 0) {
			ImageView imageView = (ImageView) holder.getView(R.id.ItemImage);

			String imageUrl = String.valueOf(data.get(position)
					.getDownload_url());

			holder.displayImages(R.id.ItemImage, imageUrl);

			holder.setText(R.id.tag, data.get(position).getTag());

			setClick(data, holder.getConvertView(), position);
		}
		return holder.getConvertView();
	}

	private ViewHolder getViewHolder(int position, View convertView,
			ViewGroup parent) {
		return ViewHolder.get(context, convertView, parent, mItemLayoutId,
				position);
	}

	/**
	 * 增加条目
	 * 
	 * @param object
	 */
	public void addItem(ImageData data) {
		this.data.add(data);
	}

	/**
	 * 停止图片缓存
	 */
	public void stopAndclear() {
		if (holder != null) {
			holder.stopAndclear();
		}
	}

	/**
	 * 绑定点击事件
	 * 
	 * @param data
	 * @param convertView
	 * @param position
	 */
	public void setClick(final List<ImageData> data, View convertView,
			final int position) {
		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String imageUrl = data.get(position).getDownload_url();
				Intent intent = new Intent(context, ImageScaleActivity.class);
				if ("null".equals(imageUrl)) {
					imageUrl = "http://h.hiphotos.baidu.com/image/pic/item/4bed2e738bd4b31c4859e0ba85d6277f9e2ff84e.jpg";
				}
				intent.putExtra("filePath", imageUrl);
				context.startActivity(intent);

			}
		});
	}

}
